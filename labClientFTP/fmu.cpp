//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	IdFTP->Host = "46.188.34.242";
	IdFTP->Username = "test";
	IdFTP->Password = "test";
	IdFTP->Passive = true;
	IdFTP->Connect();
	IdFTP->ChangeDir("/ftp/");
	IdFTP->List(lb->Items,"",false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	UnicodeString xExt = Ioutils::TPath::GetExtension(Item->Text);
	FFileName = Item->Text;
	TMemoryStream *x = new TMemoryStream;
	try {
		if (xExt == ".txt") {
			tc->ActiveTab = tiText;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			me->Lines->LoadFromStream(x);

		} else
		if (xExt == ".png" || xExt == ".jpg") {
			tc->ActiveTab = tiImage;
			IdFTP->Get(Item->Text, x, true);
			im->Bitmap->LoadFromStream(x);
		} else
		tc->ActiveTab = tiOther;
		}
		__finally {
		delete x;
		}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	UnicodeString xFN = Ioutils::TPath::GetFileName(FFileName);
	if (SaveDialog1->Execute())
	{
		TMemoryStream *x = new TMemoryStream;
		try {
			IdFTP->Get(FFileName, x, true);
			x->SaveToFile(SaveDialog1->FileName);
		}
		__finally {
			delete x;
		}

	}

}
//---------------------------------------------------------------------------
