//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.IOUtils.hpp>
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageJSON.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------


class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDMemTable *taList;
	TImageList *il;
	TFDStanStorageJSONLink *FDStanStorageJSONLink1;
	TStringField *taListDetail;
	TStringField *taListText;
	TStringField *taListHeaderText;
	TIntegerField *taListImageIndex;
	TBooleanField *taListCheckmark;
	TIntegerField *taListPrice;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall taListAfterInsert(TDataSet *DataSet);
	void __fastcall taListAfterPost(TDataSet *DataSet);
private:
	UnicodeString FFileName;	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
const UnicodeString cNameDB = "labShoppingList.json";
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
