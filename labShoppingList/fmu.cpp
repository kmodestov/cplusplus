//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	if (sbEdit->IsPressed) {
		dm->taList->Edit();
		tc->GotoVisibleTab(tiItem->Index);
	} else {
		dm->taList->Edit();
		dm->taListCheckmark->Value = ! dm->taListCheckmark->Value;
		dm->taList->Post();
		lv->Resize();

	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvUpdateObjects(TObject * const Sender, TListViewItem * const AItem)

{
	if (dm->taList->State == dsBrowse) {
		Variant x;
		x = dm->taList->Lookup(dm->taListText->FieldName, AItem->Text, dm->taListCheckmark->FieldName);
		if (! x.IsNull()) {
			AItem->Objects->AccessoryObject->Visible = x.As<bool>();

		}

	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	dm->taList->Append();
    tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button6Click(TObject *Sender)
{
    buCancelClick(this);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button11Click(TObject *Sender)
{
	dm->taList->Post();
	lv->Resize();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buCancelClick(TObject *Sender)
{
	dm->taList->Cancel();
	lv->Resize();
    tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button10Click(TObject *Sender)
{
	Glyph1->ImageIndex = ((int)Glyph1->ImageIndex <= 0) ?
		dm->il->Count - 1 : (int)Glyph1->ImageIndex - 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button9Click(TObject *Sender)
{
	Glyph1->ImageIndex = ((int)Glyph1->ImageIndex >= dm->il->Count - 1) ?
		0 : (int)Glyph1->ImageIndex + 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button8Click(TObject *Sender)
{
    Glyph1->ImageIndex = -1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Glyph1Changed(TObject *Sender)
{
	if (dm->taList->State == TDataSetState::dsInsert ||
		dm->taList->State == TDataSetState::dsEdit) {
		dm->taListImageIndex->Value = Glyph1->ImageIndex;

	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button7Click(TObject *Sender)
{
		dm->taList->Delete();
		tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
