//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TSpeedButton *sbCheck;
	TSpeedButton *sbEdit;
	TLabel *Label1;
	TTabControl *tc;
	TButton *Button1;
	TButton *Button2;
	TTabItem *tiList;
	TTabItem *tiItem;
	TListView *lv;
	TMultiView *MultiView1;
	TListBox *ListBox1;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TToolBar *ToolBar2;
	TButton *Button6;
	TButton *Button7;
	TLabel *Label2;
	TScrollBox *ScrollBox1;
	TGridPanelLayout *GridPanelLayout1;
	TLayout *Layout1;
	TLabel *Label3;
	TGlyph *Glyph1;
	TLabel *Label4;
	TEdit *Edit1;
	TLabel *Label5;
	TComboBox *ComboBox1;
	TLabel *Label6;
	TMemo *Memo1;
	TLabel *Label7;
	TSwitch *Switch1;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button11;
	TButton *buCancel;
	TButton *Button13;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldImageIndex;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField3;
	TEdit *Edit2;
	TLinkControlToField *LinkControlToField4;
	TLabel *Label8;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall lvUpdateObjects(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall buCancelClick(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Glyph1Changed(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
