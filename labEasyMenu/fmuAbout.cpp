//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmuAbout.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmAbout *fmAbout;
//---------------------------------------------------------------------------
__fastcall TfmAbout::TfmAbout(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmAbout::Button1Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
