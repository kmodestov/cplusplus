//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "fmuAbout.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiPlay;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    tc->ActiveTab = tiHelp;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	fmAbout->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button6Click(TObject *Sender)
{
    tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if ((tc->ActiveTab != tiMain) && (Key = vkHardwareBack)) {
		tc->ActiveTab = tiMain;
		Key=0;


	}
}
//---------------------------------------------------------------------------



void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

