//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Filter.Effects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TImage *Image1;
	TBlurEffect *BlurEffect1;
	THueAdjustEffect *HueAdjustEffect1;
	TToonEffect *ToonEffect1;
	TSepiaEffect *SepiaEffect1;
	TBloodTransitionEffect *BloodTransitionEffect1;
	TImage *Image2;
	TSharpenEffect *SharpenEffect1;
	TContrastEffect *ContrastEffect1;
	TImage *Image3;
	TImage *Image4;
	TWiggleTransitionEffect *WiggleTransitionEffect1;
	TRippleTransitionEffect *RippleTransitionEffect1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
