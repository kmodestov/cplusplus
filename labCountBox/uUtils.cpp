//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "uUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
int* RandomArrayUnique(int aMax, int aCount)
{
	int *xResult = new int(aCount);
	TList__1<int> *xTemp = new TList__1<int>;
	xTemp->Capacity = aCount;
	try {
		for (int i = 0; i < aCount; i++) {
				int x = Random(aMax);
				while (xTemp->IndexOf(x) != -1) {
				x++;
				if (x >= aMax)
					x = 0;

				}
				xTemp->Add(x);
				xResult[i] = x;
		}
	}
	__finally {
			delete xTemp;
	}
    return xResult;
}
//---------------------------------------------------------------------------
