//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "uUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FListBox = new TList;
	for (int i = 1; i <= cMaxBox; i++) {
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));

	}
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (double)30/(24*60*60);
	tmPlay->Enabled = true;
    DoContinue();
}
void Tfm::DoContinue()
{
	for (int i = 0; i < cMaxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;

	}
	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Lightblue;


	}
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if (xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;
	for (int i = 0; i < cMaxAnswer; i++) {
		((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i);

	}


}
void Tfm::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong > 5) {/*TODO*/}
	DoContinue();

}
void Tfm::DoFinish()
{
	tmPlay->Enabled = false;
}

//---------------------------------------------------------------------------

void __fastcall Tfm::buAnswer1Click(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss",x);
	if (x <=0)
		DoFinish();

}

//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tmPlay->Enabled = false;
	DoReset();
}
//---------------------------------------------------------------------------

