//---------------------------------------------------------------------------

#ifndef uUtilsH
#define uUtilsH
//---------------------------------------------------------------------------
#include <FMX.Controls.Presentation.hpp>
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
private:

public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
int* RandomArrayUnique(int aMax, int aCount);
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
