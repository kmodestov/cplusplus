//---------------------------------------------------------------------------

#ifndef fmH
#define fmH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *Label1;
	TProgressBar *pb;
	TTabControl *tc;
	TTabItem *tiStart;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *Button1;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TLabel *Label2;
	TButton *Button2;
	TButton *Button6;
	TLabel *Label3;
	TButton *Button7;
	TButton *Button8;
	TLabel *Label4;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TImage *Image1;
	TMemo *me;
	TButton *Button12;
	TButton *bu1;
	TButton *bu2;
	TButton *bu3;
	TStyleBook *StyleBook1;
	void __fastcall bu1Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
