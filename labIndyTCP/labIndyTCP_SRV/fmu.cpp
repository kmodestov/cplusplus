//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{

	Glyph1->ImageIndex = ((int)Glyph1->ImageIndex <= 0) ? il->Count - 1 :(int)Glyph1->ImageIndex -1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Glyph1->Images = il;
	Glyph1->ImageIndex = Random(il->Count);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Glyph1->ImageIndex = ((int)Glyph1->ImageIndex >= il->Count-1) ? 0 : (int)Glyph1->ImageIndex +1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	IdTCPServer1->Active = true;
    me->Lines->Add("Active = true");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	IdTCPServer1->Active = false;
    me->Lines->Add("Active = false");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::IdTCPServer1Connect(TIdContext *AContext)
{
	me->Lines->Add(Format("[%s] - Client connected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
	));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::IdTCPServer1Disconnect(TIdContext *AContext)
{
    me->Lines->Add(Format("[%s] - Client disconnected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
	));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::IdTCPServer1Execute(TIdContext *AContext)
{
	UnicodeString x = AContext->Connection->Socket->ReadLn();
	me->Lines->Add("Input = " + x);
	if (x == "time") {
		AContext->Connection->Socket->WriteLn(TimeToStr(Now()));


	}
	if (x == "str") {
		AContext->Connection->Socket->WriteLn(edStr->Text, IndyTextEncoding_UTF8());

	}
	if (x == "about") {
		AContext->Connection->Socket->WriteLn("�������� �.�.", IndyTextEncoding_UTF8());

	}
	if (x == "image") {
		TMemoryStream *x = new TMemoryStream();

		try {
			//Glyph1->Canvas->Bitmap->SaveToStream(x);
			Image1->Bitmap->SaveToStream(x);
			x->Seek(0, 0);
			AContext->Connection->Socket->Write(x->Size);
			AContext->Connection->Socket->Write(x);

		}
		__finally {
			delete x;
		}

	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	ShowMessage("�������� �.�.");
}
//---------------------------------------------------------------------------

