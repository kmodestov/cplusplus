//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acConnectExecute(TObject *Sender)
{
	IdTCPClient1->Host = edHost->Text;
	IdTCPClient1->Port = StrToInt(edPort->Text);
    IdTCPClient1->Connect();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acDisconnectExecute(TObject *Sender)
{
	IdTCPClient1->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetTimeExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("time");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn();
	me->Lines->Add(x);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetStrExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("str");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn(IndyTextEncoding_UTF8());
	me->Lines->Add(x);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::acGetImageExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("image");

	TMemoryStream *x = new TMemoryStream();
	try {
		int xSize = IdTCPClient1->Socket->ReadInt64();
		IdTCPClient1->Socket->ReadStream(x, xSize);
		Image1->Bitmap->LoadFromStream(x);

	}
	__finally {
        delete x;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ActionList1Update(TBasicAction *Action, bool &Handled)
{
	edHost->Enabled = !IdTCPClient1->Connected();
	edPort->Enabled = !IdTCPClient1->Connected();
	acConnect->Enabled = !IdTCPClient1->Connected();
	acDisconnect->Enabled = IdTCPClient1->Connected();
	acGetTime->Enabled = IdTCPClient1->Connected();
	acGetStr->Enabled = IdTCPClient1->Connected();
	acGetImage->Enabled = IdTCPClient1->Connected();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::acAboutExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("about");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn(IndyTextEncoding_UTF8());
    me->Lines->Add(x);
}
//---------------------------------------------------------------------------
