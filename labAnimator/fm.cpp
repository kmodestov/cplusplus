//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	laGroup->Position-> X = - laGroup->Width;
	laFIO->Position->X = this->Width + laGroup->Width;
	laPresents->Position->X = - laPresents->Width;
	TAnimator::AnimateIntWait(laGroup,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(laFIO,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(laPresents,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	laName->AutoSize = true;
	laName->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(laName,"TextSettings.Font.Size",96,1,TAnimationType::Out, TInterpolationType::Linear);
    laName->AutoSize = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
	int y = GridPanelLayout1->Position->Y;
	GridPanelLayout1->Position->Y = -GridPanelLayout1->Height;
	TAnimator::AnimateIntWait(GridPanelLayout1,"Position.Y",y,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
