//---------------------------------------------------------------------------

#ifndef fm1H
#define fm1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *buDel10sec;
	TButton *buPause;
	TButton *buRestart;
	TLabel *laTime;
	TTimer *tm;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buDel10secClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
private:
		 double FTimeValue;
		 bool FTimePause;	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
