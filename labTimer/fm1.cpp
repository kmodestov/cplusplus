//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fm1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buRestartClick(TObject *Sender)
{
	FTimeValue = Time().Val + (double)30/(24*60*60);
	FTimePause = false;
	tm->Enabled = true;

}
//---------------------------------------------------------------------------
void __fastcall TForm1::buPauseClick(TObject *Sender)
{
	if (FTimePause) {
	FTimeValue = Time().Val + FTimeValue;
	FTimePause = false;
	tm->Enabled = true;
	buPause->Text = L"�����";
}   else {
	tm->Enabled = false;
	FTimePause = true;
	FTimeValue = FTimeValue - Time().Val;
	buPause->Text = L"������";
}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buDel10secClick(TObject *Sender)
{
	FTimeValue = FTimeValue - (double)10/(24*60*60);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tmTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	if (x <= 0) {
		x = 0;
		tm->Enabled = false;


	}
	//laTime->Text = FormatDateTime("nn:ss", x);
	laTime->Text = FormatDateTime("nn:ss.zzz", x).Delete(7,2); + "."
					+ FormatDateTime("zzz", x).SubString(0,1);
}
//---------------------------------------------------------------------------
