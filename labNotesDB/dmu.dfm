object dm: Tdm
  OldCreateOrder = False
  Height = 265
  Width = 354
  object FDConnection: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\VD\Documents\projradstudio1\labNotesDB\Notes.d' +
        'b'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 40
    Top = 48
  end
  object taNotes: TFDTable
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 72
    Top = 168
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 500
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 232
    Top = 56
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 256
    Top = 160
  end
end
