//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormShow(TObject *Sender)
{
    dm->FDConnection->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	dm->taNotes->Append();
    tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	dm->taNotes->Cancel();
    tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button6Click(TObject *Sender)
{
	dm->taNotes->Delete();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lwItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
	ShowMessage("�������� �.�., 174-352");
}
//---------------------------------------------------------------------------

