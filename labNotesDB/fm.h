//---------------------------------------------------------------------------

#ifndef fmH
#define fmH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *Button2;
	TListView *ListView2;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TButton *Button3;
	TButton *Button1;
	TLayout *Layout1;
	TLabel *Label3;
	TEdit *Edit1;
	TLabel *Label4;
	TTrackBar *TrackBar1;
	TLabel *Label5;
	TToolBar *ToolBar3;
	TMemo *Memo1;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
