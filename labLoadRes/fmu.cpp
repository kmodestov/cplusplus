//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
void LoadResourceToImage(UnicodeString aResName, TBitmap* xResult)
{
	TResourceStream* x;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
		xResult->LoadFromStream(x);
	}
	__finally {
		delete x;
	}
}
UnicodeString LoadResourceToText(UnicodeString aResName)
{
	TResourceStream* x;
	TStringStream* x88;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
		x88 = new TStringStream("", TEncoding::Unicode, true);
		try {
			x88->LoadFromStream(x);
			return x88->DataString;
		}
		__finally {
			delete x88;
		}
	}
	__finally{
		delete x;
	}
}

//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	LoadResourceToImage("Picture1", im->Bitmap);
	me->Lines->Text = LoadResourceToText("vopros");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	LoadResourceToImage("Picture2", im->Bitmap);
	me->Lines->Text = LoadResourceToText("grust");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	im->Bitmap->SetSize(0,0);
	me->Lines->Clear();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
    ShowMessage("�������� �.�.");
}
//---------------------------------------------------------------------------
