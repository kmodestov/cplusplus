//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buNewClick(TObject *Sender)
{
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->Width = 50 + random(100);
	x->Height = 50 + random(100);
	x->Position->X = random(ly->Width - x->Width);
	x->Position->Y = random(ly->Height - x->Height);
	x->RotationAngle = RandomRange(-40,40);
	x->OnMouseDown = SelectionAllMouseDown;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = dm->il;
	xGlyph->ImageIndex = random(dm->il->Count);
	ResetSelection(x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y)
{
	ResetSelection(Sender);
}

void Tfm::ResetSelection(TObject *Sender)
{   FSel = dynamic_cast<TSelection*>(Sender);
	TSelection *x;
	for (int i=0; i < ly->ComponentCount; i++) {
		x = dynamic_cast<TSelection*>(ly->Components[i]);
		if (x) {
		x->HideSelection = (x !=FSel);

		}
	}
	tbOptions->Visible = (FSel != NULL);
	if (tbOptions->Visible) {
		tbRotation->Value = FSel->RotationAngle;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buClearClick(TObject *Sender)
{
	ResetSelection(ly);
	for (int i = ly->ComponentCount-1;i >= 0; i--) {
		if (dynamic_cast<TSelection*>(ly->Components[i])){
			ly->RemoveObject(i);

		}
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	ResetSelection(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
    ResetSelection(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buLeftClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex <= 0) ? dm->il->Count - 1 :(int)x->ImageIndex -1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRightClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex >= dm->il->Count-1) ? 0 : (int)x->ImageIndex +1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buUpClick(TObject *Sender)
{
	FSel->BringToFront();
    FSel->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDownClick(TObject *Sender)
{
	FSel->SendToBack();
    FSel->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tbRotationChange(TObject *Sender)
{
    FSel->RotationAngle = tbRotation->Value;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDelClick(TObject *Sender)
{
	FSel->DisposeOf();
	FSel= NULL;
	ResetSelection(FSel);

}
//---------------------------------------------------------------------------
void __fastcall Tfm::buInfoClick(TObject *Sender)
{
	ShowMessage("�������� �.�.");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRectangleClick(TObject *Sender)
{
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->Width = 50 + random(100);
	x->Height = 50 + random(100);
	x->Position->X = random(ly->Width - x->Width);
	x->Position->Y = random(ly->Height - x->Height);
	x->RotationAngle = RandomRange(-40,40);
	x->OnMouseDown = RectangleMouseDown;
	TRectangle *xRectangle = new TRectangle(x);
	xRectangle->Parent = x;
	xRectangle->HitTest = false;
	xRectangle->Align = TAlignLayout::Client;
	xRectangle->Stroke->Thickness = Random(5);
	xRectangle->Stroke->Color = TAlphaColorF::Create(Random(256), Random(256), Random(256)).ToAlphaColor();
	xRectangle->Fill->Color = TAlphaColorF::Create(Random(256), Random(256), Random(256)).ToAlphaColor();
	ResetSelection(x);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::RectangleMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y)
{
	ResetSelection(Sender);
}
//---------------------------------------------------------------------------
