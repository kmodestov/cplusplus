//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.UIConsts.hpp>
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>

//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbTop;
	TButton *buNew;
	TButton *buClear;
	TButton *buInfo;
	TLayout *ly;
	TToolBar *tbOptions;
	TButton *buLeft;
	TButton *buDown;
	TButton *buUp;
	TButton *buRight;
	TButton *buDel;
	TTrackBar *tbRotation;
	TButton *buRectangle;
	void __fastcall buNewClick(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buLeftClick(TObject *Sender);
	void __fastcall buRightClick(TObject *Sender);
	void __fastcall buUpClick(TObject *Sender);
	void __fastcall buDownClick(TObject *Sender);
	void __fastcall tbRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buRectangleClick(TObject *Sender);
	void __fastcall RectangleMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y);
private:
	TSelection *FSel;
	void ResetSelection(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
